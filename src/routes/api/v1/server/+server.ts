import { json } from "@sveltejs/kit";
import packageJson from "../../../../../package.json";

export function GET() {
  return json({
    name: packageJson.name,
    version: packageJson.version,
    description: packageJson.description,
  });
}

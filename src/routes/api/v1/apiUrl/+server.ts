import { json } from '@sveltejs/kit';

import { API_URL } from "$env/static/private"

export async function GET() {
  return json({
    apiUrl: API_URL
  });
}

import "../app.scss";
import { loadTheme } from "$lib/js/core/theme";
import { isLoggedIn } from "$lib/js/auth/loggedIn";
import { loadUser } from "$lib/js/api/user/load";
import { loggedIn } from "$lib/js/auth/store";

// disable server-side rendering for this layout
export const ssr = false;

export const load = () => {
  loadTheme();
  loggedIn.set(isLoggedIn());

  loadUser();
}

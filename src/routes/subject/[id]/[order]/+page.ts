import { fetchResource } from "$lib/js/api/fetch";
import { getSchedule } from "$lib/js/api/user/schedule";
import { error } from "@sveltejs/kit";

export async function load({ params }) {
  const schedule = await getSchedule()
  console.log("Loading subject with id: " + params.id + " and order: " + params.order)

  for (const day in Object.keys(schedule["days"])) {
    for (const subject in Object.keys(schedule["days"][day]["schedules"])) {
      const currentSubject = schedule["days"][day]["schedules"][subject]
      if (currentSubject["scheduledHourId"] == params.id && currentSubject["scheduledHourOrder"] == params.order) {
        return currentSubject
      }
    }
  }

  throw error(404, "Předmět nebyl nalezen.")
}

import { fetchResource } from "$lib/js/api/fetch";

export async function load() {
  const config = await fetchResource("resol/info", {});
  const json = await config.json();

  return json
}

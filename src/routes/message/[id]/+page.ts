import { fetchResource } from "$lib/js/api/fetch";
import { error } from "@sveltejs/kit";

export async function load({ params }) {
  const response = await fetchResource("messages", {
    method: "GET",
    headers: {
      "sol-url": "{solUrl}",
      "Authorization": "{authHeader}"
    }
  })

  const json = await response.json()

  let message: any = {};

  for (const key in Object.keys(json["messages"])) {
    if (json["messages"][key]["id"] === params.id) {
      message = json["messages"][key]
    }
  }

  if (Object.keys(message).length === 0) {
    throw error(404, "Zpráva nebyla nalezena.")
  }

  return {
    message: message
  }
}

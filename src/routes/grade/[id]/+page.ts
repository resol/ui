import { fetchResource } from "$lib/js/api/fetch";

export async function load({ params }) {
  const response = await fetchResource(`grade?gradeId=${params.id}&studentId={studentId}`, {
    method: "GET",
    headers: {
      "sol-url": "{solUrl}",
      "Authorization": "{authHeader}"
    }
  })

  const json = await response.json()

  return {
    grade: json
  }
}

import { fetchResource } from "$lib/js/api/fetch";

export async function load() {
  const response = await fetchResource(`grades?studentId={studentId}`, {
    method: "GET",
    headers: {
      "sol-url": "{solUrl}",
      "Authorization": "{authHeader}"
    }
  })

  const json = await response.json()

  return {
    grades: json["marks"],
    subjects: json["subjects"],
  }
}

import { getBehavior } from "$lib/js/api/user/behavior";
import { getGrades } from "$lib/js/api/user/grades";
import { getHomework } from "$lib/js/api/user/homework";
import { getMessages } from "$lib/js/api/user/messages";
import { getSchedule } from "$lib/js/api/user/schedule";
import { isLoggedIn } from "$lib/js/auth/loggedIn";
import { behaviorLoading, gradesLoading, homeworkLoading, messagesLoading } from "$lib/js/misc/loaders/store";
import type { Writable } from "svelte/store";

export const prerender = true

export async function load() {
  if (!isLoggedIn()) return

  async function loadModule(loader: Writable<boolean>, moduleFunction: () => Promise<any[]>) {
    loader.set(true);
    const object = await moduleFunction();
    loader.set(false);

    return object;
  }

  // messages
  const messages = loadModule(messagesLoading, getMessages)

  // grades
  const grades = loadModule(gradesLoading, getGrades)

  // homework
  const homework = loadModule(homeworkLoading, getHomework)

  // behavior 
  const behavior = loadModule(behaviorLoading, getBehavior)

  // schedule 
  const schedule = getSchedule()

  return {
    messages,
    grades,
    homework,
    behavior,
    schedule
  }
}

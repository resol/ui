import { goto } from "$app/navigation";

import { loggedIn } from "$lib/js/auth/store";

export function logout() {
  localStorage.removeItem("authHeader");
  localStorage.removeItem("solUrl");
  localStorage.removeItem("fullName");
  localStorage.removeItem("userTypeText");
  loggedIn.set(false);

  goto("/login");
}

export function isLoggedIn(): boolean {
  return localStorage.getItem("authHeader") !== null || localStorage.getItem("solUrl") !== null;
}

import { writable } from "svelte/store";

export const loggedIn = writable(false);

export const username = writable("jan novak");
export const userClass = writable("I. D.");

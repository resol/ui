export function getGradeColor(value: string, type: string) {
  switch (value) {
    case "1":
      return `${type}-grade1`;
    case "2":
      return `${type}-grade2`;
    case "3":
      return `${type}-grade3`;
    case "4":
      return `${type}-grade4`;
    case "5":
      return `${type}-grade5`;
    default:
      return `${type}-secondary`;
  }
}


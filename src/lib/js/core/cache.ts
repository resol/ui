import { fetchResource } from "../api/fetch";

export async function deleteCache(): Promise<Error | null> {
  const response = await fetchResource("cache/clean", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": "{authHeader}",
      "sol-url": "{solUrl}"
    }
  }, {
    errorCallback: () => {/**/ }
  })

  if (response.status === 200) {
    return null
  } else {
    if (response) {
      const json = await response.json()

      if (json["error"]) {
        throw new Error(json["specific"])
      }

      throw new Error("Nepodařilo se smazat cache. Zkuste to prosím znovu. " + response.status)
    }
    throw new Error("Nepodařilo se smazat cache. Zkuste to prosím znovu. " + response.status)
  }
}

import { writable } from "svelte/store";

export const footerName = writable("");
export const footerVersion = writable("");

export async function loadFooter() {
  const response = await fetch("/api/v1/server");
  const json = await response.json();

  footerName.set(json.name);
  footerVersion.set(json.version);
}

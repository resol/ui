import { fetchResource } from "../fetch";

import { convertDateToString } from "$lib/js/core/date";

export async function getBehavior() {
  const res = await fetchResource(`behavior?studentId={studentId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  });
  const data = await res.json();

  const cellObject = [];

  for (const behaviorId in Object.keys(data["behaviors"])) {
    // console.log(data["messages"][message])
    const behavior = data["behaviors"][behaviorId]

    cellObject.push({
      "date": convertDateToString(new Date(behavior["date"])),
      "name": behavior["createName"],
      "description": behavior["kindOfBehaviorName"],
      "link": `/behavior/${behavior["behaviorId"]}`,
      "read": behavior["signedDate"] ?? false
    })
  }

  // console.log(cellObject)
  return cellObject;
}

import { convertDateToString } from "$lib/js/core/date";
import { getGradeColor } from "$lib/js/util/gradeColor";
import { fetchResource } from "../fetch";

export async function getGrades() {
  const res = await fetchResource(`grades?studentId={studentId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`,
      "studentId": "{studentId}"
    }
  });
  const data = await res.json();

  const rowObject: any[] = [];
  const subjectMap = new Map();

  for (const subject in Object.keys(data["subjects"])) {
    subjectMap.set(data["subjects"][subject]["id"], data["subjects"][subject]["abbrev"])
  }

  for (const grade in Object.keys(data["marks"])) {
    const mark = data["marks"][grade]

    if (mark["markText"] == "-") continue

    rowObject.push({
      "date": convertDateToString(new Date(mark["markDate"])),
      "name": subjectMap.get(mark["subjectId"]),
      "description": mark["markText"],
      "link": `/grade/${mark["id"]}`,
      "read": mark["signedDate"] ?? false,
      "color": getGradeColor(mark["markText"], "text")
    })
  }

  // console.log(cellObject)
  return rowObject;
}

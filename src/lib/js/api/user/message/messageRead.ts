import { createBubbleError } from "../../errors/bubble";
import { fetchResource } from "../../fetch";

export async function setMessageRead(messageId: string) {
  const response = await fetchResource(`messageRead?messageId=${messageId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  })

  if (response.status === 200) {
    return true
  } else {
    createBubbleError(new Error("Nepodařilo se označit zprávu jako přečtenou. Zkuste to prosím znovu. " + response.status))
    return false
  }
}

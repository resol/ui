import { createBubbleError } from "../../errors/bubble";
import { fetchResource } from "../../fetch";

export async function deleteMessage(messageId: string) {
  const response = await fetchResource(`messages?messageId=${messageId}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `{authHeader}`,
      "sol-url": `{solUrl}`
    }
  })

  if (response.status === 200) {
    return true
  } else {
    createBubbleError(new Error("Nepodařilo se smazat zprávu. Zkuste to prosím znovu. " + response.status))
    return false
  }
}
